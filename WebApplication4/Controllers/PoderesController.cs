﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication4.BLL;
using WebApplication4.Models;
using WebApplication4.util;

namespace WebApplication4.Controllers
{
    public class PoderesController : Controller
    {
        static private HeroisService heroisService = ServiceFactory.GetCurrentHeroisService();

        // GET: Poderes
        public ActionResult Index()
        {
            return View(heroisService.listarPoderes());
        }

        // GET: Poderes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Poder poder = heroisService.obterPoder((int)id);
            if (poder == null)
            {
                return HttpNotFound();
            }
            return View(poder);
        }

        // GET: Poderes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Poderes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nome")] Poder poder)
        {
            if (ModelState.IsValid)
            {
                heroisService.incluirPoder(poder);
                return RedirectToAction("Index");
            }

            return View(poder);
        }

        // GET: Poderes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Poder poder = heroisService.obterPoder((int)id);
            if (poder == null)
            {
                return HttpNotFound();
            }
            return View(poder);
        }

        // POST: Poderes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nome")] Poder poder)
        {
            if (ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }
            return View(poder);
        }

        // GET: Poderes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Poder poder = heroisService.obterPoder((int)id);
            if (poder == null)
            {
                return HttpNotFound();
            }
            return View(poder);
        }

        // POST: Poderes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            heroisService.excluirPoder(id);
            return RedirectToAction("Index");
        }
    }
}

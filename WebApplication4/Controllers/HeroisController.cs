﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using WebApplication4.BLL;
using WebApplication4.Models;
using WebApplication4.util;
using System.Data;
using System.Linq;

namespace WebApplication4.Controllers
{
    public class HeroisController : Controller
    {
        static private HeroisService heroisService = ServiceFactory.GetCurrentHeroisService();

        // GET: Herois
        public ActionResult Index()
        {
            return View(heroisService.listarHerois());
        }

        // GET: Herois/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Heroi heroi = heroisService.obterHeroi((int)id);
            if (heroi == null)
            {
                return HttpNotFound();
            }
            heroi.PoderesId = (from p in heroi.Poderes select p.Id).ToArray();
            ViewBag.ListaPoderes = heroisService.listarPoderes();
            return View(heroi);
        }

        // GET: Herois/Create
        public ActionResult Create()
        {
            Heroi heroi = new Heroi
            {
                PoderesId = new int[0]
            };
            ViewBag.EquipeId = new SelectList(heroisService.listarEquipes(), "Id", "Nome");
            ViewBag.ListaPoderes = heroisService.listarPoderes();
            return View(heroi);
        }

        // POST: Herois/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Heroi heroi)
        {
            if (ModelState.IsValid)
            {
                heroisService.incluirHeroi(heroi);
                return RedirectToAction("Index");
            }

            ViewBag.EquipeId = new SelectList(heroisService.listarEquipes(), "Id", "Nome", heroi.EquipeId);
            return View(heroi);
        }

        // GET: Herois/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Heroi heroi = heroisService.obterHeroi((int)id);
            if (heroi == null)
            {
                return HttpNotFound();
            }
            heroi.PoderesId = (from p in heroi.Poderes select p.Id).ToArray();

            ViewBag.EquipeId = new SelectList(heroisService.listarEquipes(), "Id", "Nome", heroi.EquipeId);
            ViewBag.ListaPoderes = heroisService.listarPoderes();
            return View(heroi);
        }
        // POST: Herois/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Heroi heroi)
        {
            if (ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }
            ViewBag.EquipeId = new SelectList(heroisService.listarEquipes(), "Id", "Nome", heroi.EquipeId);
            return View(heroi);
        }

        // GET: Herois/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Heroi heroi = heroisService.obterHeroi((int)id);
            if (heroi == null)
            {
                return HttpNotFound();
            }
            return View(heroi);
        }

        // POST: Herois/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            heroisService.excluirHeroi(id);
            return RedirectToAction("Index");
        }

        public ActionResult Relatorio()
        {
            // gerar o relatório aqui
            return null;
        }
    }
}

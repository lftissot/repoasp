﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication4.DAL;
using WebApplication4.Models;

namespace WebApplication4.BLL
{
    public class HeroisService
    {
        private static HeroisDAO heroisDAO = new HeroisDAO();
        private static EquipesDAO equipesDAO = new EquipesDAO();
        private static PoderesDAO poderesDAO = new PoderesDAO();
        public List<Heroi> listarHerois()
        {
            return heroisDAO.listar();
        }
        public Heroi obterHeroi(int id)
        {
            return heroisDAO.obter(id);
        }
        public void incluirHeroi(Heroi heroi)
        {
            if (heroi.PoderesId != null)
            {
                heroi.Poderes = (from p in listarPoderes() where heroi.PoderesId.Contains(p.Id) select p).ToList();
            }
            heroisDAO.incluir(heroi);
        }
        public void alterarHeroi(Heroi heroi)
        {
            if (heroi.PoderesId != null)
            {
                heroi.Poderes = (from p in listarPoderes() where heroi.PoderesId.Contains(p.Id) select p).ToList();
            }
            heroisDAO.alterar(heroi);
        }
        public void excluirHeroi(int id)
        {
            heroisDAO.excluir(id);
        }





        public List<Equipe> listarEquipes()
        {
            return equipesDAO.listar();
        }
        public Equipe obterEquipe(int id)
        {
            return equipesDAO.obter(id);
        }
        public void incluirEquipe(Equipe equipe)
        {
            equipesDAO.incluir(equipe);
        }
        public void alterarEquipe(Equipe equipe)
        {
            equipesDAO.alterar(equipe);
        }
        public void excluirEquipe(int id)
        {
            equipesDAO.excluir(id);
        }



        public List<Poder> listarPoderes()
        {
            return poderesDAO.listar();
        }
        public Poder obterPoder(int id)
        {
            return poderesDAO.obter(id);
        }
        public void incluirPoder(Poder poder)
        {
            poderesDAO.incluir(poder);
        }
        public void alterarPoder(Poder poder)
        {
            poderesDAO.alterar(poder);
        }
        public void excluirPoder(int id)
        {
            poderesDAO.excluir(id);
        }

    }
}
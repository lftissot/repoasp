﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApplication4.Models;
using WebApplication4.util;

namespace WebApplication4.DAL
{
    public class PoderesDAO
    {
        public List<Poder> listar()
        {
            Model1 db = DBContextFactoryRegistry.GetCurrent();
            var poderes = db.Poderes;
            return poderes.ToList();
        }
        public Poder obter(int id)
        {
            Model1 db = DBContextFactoryRegistry.GetCurrent();
        }
        public void incluir(Poder poder)
        {
            Model1 db = DBContextFactoryRegistry.GetCurrent();
        }
        public void alterar(Poder poder)
        {
            Model1 db = DBContextFactoryRegistry.GetCurrent();
        }
        public void excluir(int id)
        {
            Model1 db = DBContextFactoryRegistry.GetCurrent();
        }
    }
}
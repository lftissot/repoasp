﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApplication4.Models;
using WebApplication4.util;

namespace WebApplication4.DAL
{
    public class EquipesDAO
    {
        public List<Equipe> listar()
        {
            Model1 db = DBContextFactoryRegistry.GetCurrent();
            var equipes = db.Equipes;
            return equipes.ToList();
        }
        public Equipe obter(int id)
        {
            Model1 db = DBContextFactoryRegistry.GetCurrent();
            Equipe equipe = db.Equipes.Find(id);
            return equipe;
        }
        public void incluir(Equipe equipe)
        {
            Model1 db = DBContextFactoryRegistry.GetCurrent();
        }
        public void alterar(Equipe equipe)
        {
            Model1 db = DBContextFactoryRegistry.GetCurrent();
        }
        public void excluir(int id)
        {
            Model1 db = DBContextFactoryRegistry.GetCurrent();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using WebApplication4.Models;
using WebApplication4.util;

namespace WebApplication4.DAL
{
    public class HeroisDAO
    {
        public List<Heroi> listar()
        {
            Model1 db = DBContextFactoryRegistry.GetCurrent();
            var herois = db.Herois.Include(h => h.Equipe);
            return herois.ToList();
        }
        public Heroi obter(int id)
        {
            Model1 db = DBContextFactoryRegistry.GetCurrent();
            Heroi heroi = db.Herois.Find(id);
            return heroi;
        }
        public void incluir(Heroi heroi)
        {
            Model1 db = DBContextFactoryRegistry.GetCurrent();
        }
        public void alterar(Heroi heroi)
        {
            Model1 db = DBContextFactoryRegistry.GetCurrent();
            Heroi h = db.Herois.Find(heroi.Id);
            h.EquipeId = heroi.EquipeId;
            h.Nome = heroi.Nome;
            h.Poderes.Clear();

            if (heroi.PoderesId != null)
            {
                h.Poderes = heroi.Poderes;
            }
            db.SaveChanges();
        }
        public void excluir(int id)
        {
            Model1 db = DBContextFactoryRegistry.GetCurrent();
        }
    }
}
namespace WebApplication4.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public virtual DbSet<Atendimento> Atendimentos { get; set; }
        public virtual DbSet<Equipe> Equipes { get; set; }
        public virtual DbSet<Heroi> Herois { get; set; }
        public virtual DbSet<Poder> Poderes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Atendimento>()
                .HasMany(e => e.Herois)
                .WithMany(e => e.Atendimentos)
                .Map(m => m.ToTable("Escalacao").MapLeftKey("AtendimentoId").MapRightKey("HeroiId"));

            modelBuilder.Entity<Heroi>()
                .HasMany(e => e.Poderes)
                .WithMany(e => e.Herois)
                .Map(m => m.ToTable("Herois_Poderes").MapLeftKey("HoreiId").MapRightKey("PoderId"));
        }
    }
}

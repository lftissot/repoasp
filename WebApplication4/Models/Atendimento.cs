namespace WebApplication4.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Atendimento")]
    public partial class Atendimento
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Atendimento()
        {
            Herois = new HashSet<Heroi>();
        }

        public int Id { get; set; }

        public DateTime Quando { get; set; }

        [StringLength(100)]
        public string Descricao { get; set; }

        public DateTime? conclusao { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Heroi> Herois { get; set; }
    }
}

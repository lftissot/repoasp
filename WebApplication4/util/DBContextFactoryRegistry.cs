﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApplication4.Models;

namespace WebApplication4.util
{
    public class DBContextFactoryRegistry
    {
        public static IDBContextFactory<Model1> Factory { get; set; }
        public static Model1 GetCurrent()
        {
            return Factory.GetCurrent();
        }
    }
}
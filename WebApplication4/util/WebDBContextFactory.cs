﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplication4.util
{
    public class WebDBContextFactory<T> : IDBContextFactory<T> where T : DbContext, new()
    {
        public static string KEY = "_DBContextFactory";

        public T GetCurrent()
        {
            var db = (T)HttpContext.Current.Items[KEY];
            if (db == null)
            {
                db = new T();
                HttpContext.Current.Items[KEY] = db;
            }
            return db;
        }
        public void SetCurrent(T dbmodel)
        {
            HttpContext.Current.Items[KEY] = dbmodel;
        }
        public void End()
        {
            T db = (T)HttpContext.Current.Items[KEY];
            if (db != null)
            {
                db.Dispose();
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication4.util
{
    public class PartialViewModel
    {
        public string Source { get; set; }
        public string Title { get; set; }
    }
}
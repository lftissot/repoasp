﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication4.BLL;

namespace WebApplication4.util
{
    public class ServiceFactory
    {
        private static HeroisService heroisService = new HeroisService();
        public static HeroisService GetCurrentHeroisService() { return heroisService;  }
    }
}
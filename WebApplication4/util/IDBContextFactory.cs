﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplication4.util
{
    public interface IDBContextFactory<T> where T : DbContext
    {
        T GetCurrent();
        void SetCurrent(T value);
        void End();
    }
}